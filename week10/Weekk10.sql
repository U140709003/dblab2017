select title
from movies
where budget > 100000000 and ranking <6;

select title
from movies join genres on movies.movie_id =genres .movie_id
where rating > 8.8 and year >2009 and genre_name="Action";

select title
from movies join genres on movies.movie_id=genres.movie_id
where duration >150 and oscars >2 and genre_name="Drama";

select title
from movies
where oscars > 2 and movie_id in (
	select movie_id
	from movie_stars join stars on movie_stars.star_id=stars.star_id
	where star_name="Orlando Bloom" and movie_id in(
	select movie_id
	from movie_stars join stars on movie_stars.star_id=stars.star_id
	where star_name="Ian McKellen"
	)
);

select title
from movies
where votes > 500000 and year <2000 and movie_id in(
	select movie_id
	from movie_directors join directors on movie_directors.director_id=directors.director_id
	where director_name="Quentin Tarantino"
);



